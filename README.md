CLgrep
======
This is the source code of CLgrep, a GPU-armed string matching tool.
Currently it sitll under development.
Soon it is going to be most awesome exact string matching tool.
You need to make sure that you have install OpenCL implementation.
CLgrep is developed without any extension, and therefore it should be able to 
run with any OpenCL implementation but AMD APP SDK is suggested.

Cheers.

CLgrep is under GPL3 Lisence.
Developer is Peng Wu (xuwupeng2000@gmail.com)
